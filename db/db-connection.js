const mongoose = require('mongoose');

const KEYS = require('../configs/keys/keys');

mongoose.connect(KEYS.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('DB connected successfully'))
  .catch((err) => console.log(err));

module.exports = mongoose;
