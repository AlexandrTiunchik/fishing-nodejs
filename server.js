const Koa = require('koa');
const parser = require('koa-parser');
const koaValidator = require('koa-async-validator');
const passport = require('koa-passport');
const cors = require('koa-cors');

const errorMiddleware = require('./middleware/error.middleware');
const router = require('./routes/root');
require('./db/db-connection');
require('./middleware/passport.middleware');

const app = new Koa();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(parser());
app.use(koaValidator());
app.use(passport.initialize());
app.use(router());
app.on('error', (err, status, ctx) => (
  errorMiddleware(err, status, ctx)
));

app.listen(port, () => console.log('Server start'));
