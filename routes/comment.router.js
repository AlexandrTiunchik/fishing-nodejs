const Router = require('koa-router');
const passport = require('koa-passport');

const { ROUTERS } = require('../constants/routers.constant');
const Comment = require('../schemas/comment.schema');

const router = new Router({
  prefix: ROUTERS.COMMENT_ROUTE,
});

// @route POST api/comment/
// @desc create comment for news
// @access ADMIN, USER
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { text } = ctx.request.body;
      const { user } = ctx.state;

      const newComment = new Comment({
        user: user.id,
        news: id,
        text,
      });

      await newComment.save();

      ctx.status = 201;
      ctx.body = { message: 'Comment was created successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route DELETE api/comment/
// @desc delete news comment
// @access ADMIN, USER
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { user } = ctx.state;

      await Comment.findOneAndRemove({
        news: id,
        user: user.id,
      });

      ctx.status = 200;
      ctx.body = { message: 'Comment was deleted successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route GET api/comment/
// @desc get news comments
// @access PUBLIC
router.get(
  '/:id',
  async (ctx) => {
    try {
      const { id } = ctx.params;

      const comments = await Comment
        .find({ news: id })
        .populate({ 
          path: 'user',
          model: 'user',
          select: { loginName: 1, avatar: 1, profile: 1 },
          populate: {
            path: 'profile',
            model: 'profile',
          }
        });

      ctx.status = 200;
      ctx.body = comments;
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

module.exports = router;
