const Router = require('koa-router');
const bcrypt = require('bcrypt');
const util = require('util');

const { ROUTERS } = require('../constants/routers.constant');
const User = require('../schemas/user.schema');
const Profile = require('../schemas/profile.schema');
const loginValidation = require('../validation/login.validation');
const registrationValidation = require('../validation/registration.validation');
const createToken = require('../functions/create-token.function');

const router = new Router({
  prefix: ROUTERS.AUTHENTICATION_ROUTE,
});

/**
 * Access: Public
 * Type: Post
 * URL: /api/authentication/login
 * Login route
*/
router.post('/login', async (ctx) => {
  try {
    loginValidation(ctx);

    const errors = await ctx.validationErrors();

    if (errors) {
      const errorMessage = `There have been validation errors: ${util.inspect(errors)}`;
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const { loginName, password } = ctx.request.body;

    const user = await User.findOne({ loginName });

    if (!user) {
      const errorMessage = 'User not found';
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const isPasswordCompare = await bcrypt.compare(password, user.password);

    if (!isPasswordCompare) {
      const errorMessage = 'Password incorrect';
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const token = await createToken(user);

    ctx.status = 200;
    ctx.body = token;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

/**
 * Access: Public
 * Type: Post
 * URL: /api/authentication/registration
 * Registration route
*/
router.post('/registration', async (ctx) => {
  try {
    registrationValidation(ctx);

    const errors = await ctx.validationErrors();

    if (errors) {
      const errorMessage = `There have been validation errors: ${util.inspect(errors)}`;
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const { loginName, password, email } = ctx.request.body;

    const loginExist = await User.findOne({ loginName });

    if (loginExist) {
      const errorMessage = 'User already exist';
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const emailExist = await User.findOne({ email });

    if (emailExist) {
      const errorMessage = 'Email already exist';
      return ctx.app.emit('error', errorMessage, 400, ctx);
    }

    const newProfile = new Profile();
    const profile = await newProfile.save();

    const newUser = new User({
      profile: profile.id,
      loginName,
      password,
      email,
    });

    const salt = await bcrypt.genSalt(10);

    newUser.password = await bcrypt.hash(newUser.password, salt);

    const user = await newUser.save();

    await Profile.findByIdAndUpdate(
      profile.id,
      {
        user: user.id,
      }
    );

    const token = await createToken(user);

    ctx.status = 200;
    ctx.body = token;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

module.exports = router;
