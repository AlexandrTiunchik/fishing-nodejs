const Router = require('koa-router');
const passport = require('koa-passport');

const { ROUTERS } = require('../constants/routers.constant');
const FishSchema = require('../schemas/fish.schema');

const router = new Router({
  prefix: ROUTERS.FISH_ROUTE,
});

// @route GET api/fish/
// @desc GET fish articles
// @access PUBLIC
router.get('/', async (ctx) => {
  try {
    const articles = await FishSchema.find();

    ctx.status = 200;
    ctx.body = articles;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

// @route GET api/fish/
// @desc GET fish article
// @access PUBLIC
router.get('/:id', async (ctx) => {
  try {
    const { id } = ctx.params;

    const article = await FishSchema.findById(id);

    ctx.status = 200;
    ctx.body = article;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

// @route PUT api/fish/
// @desc Redact fish article
// @access ADMIN
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { title, text, titleImage } = ctx.request.body;

      const updateFish = {
        title,
        text,
        titleImage,
      };

      await FishSchema.findByIdAndUpdate(
        id,
        { $set: updateFish },
      );

      ctx.status = 201;
      ctx.body = { message: 'Article was update successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route POST api/fish/
// @desc Create fish article
// @access ADMIN
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { title, text, titleImage } = ctx.request.body;

      const newFish = new FishSchema({
        title,
        text,
        titleImage,
      });

      await newFish.save();

      ctx.status = 201;
      ctx.body = { message: 'Article was created successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route DELETE api/fish/
// @desc Delete fish article
// @access ADMIN
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;

      await FishSchema.findByIdAndRemove(id);

      ctx.status = 200;
      ctx.body = { message: 'Article was deleted successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

module.exports = router;
