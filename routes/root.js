const combineRouters = require('koa-combine-routers');

const newsRouter = require('./news.router');
const fishRouter = require('./fish.router');
const authenticationRouter = require('./authentication.router');
const commentRouter = require('./comment.router');
const voteRouter = require('./vote.router');
const userRouter = require('./user.router');
const profileRouter = require('./profile.router');

const router = combineRouters([
  authenticationRouter,
  newsRouter,
  fishRouter,
  commentRouter,
  voteRouter,
  userRouter,
  profileRouter,
]);

module.exports = router;
