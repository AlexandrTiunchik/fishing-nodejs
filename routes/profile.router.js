const Router = require('koa-router');
const passport = require('koa-passport');

const { ROUTERS } = require('../constants/routers.constant');
const Profile = require('../schemas/profile.schema');

const router = new Router({
  prefix: ROUTERS.PROFILE_ROUTE,
});

// @route PUT api/vote/
// @desc update profile
// @access ADMIN, USER
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const {
        name,
        surname,
        avatar,
        about,
        images,
        socialLink,
      } = ctx.request.body;

      const { id } = ctx.state.user;

      const updateProfile = {
        name,
        surname,
        avatar,
        about,
        images,
        socialLink,
      };

      const profile = await Profile.findOneAndUpdate(
        { user: id },
        { $set: updateProfile },
      );

      if (!profile) {
        const errorMessage = 'Profile not found';
        return ctx.app.emit('error', errorMessage, 400, ctx);
      }

      ctx.status = 200;
      ctx.body = { message: 'Profile was successfully updated' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
});

// @route GET api/profile/id
// @desc get profile
// @access PUBLIC
router.get(
  '/:id',
  async (ctx) => {
    try {
      const { id } = ctx.params;

      const profile = await Profile
        .findById(id)
        .populate('user');

      ctx.status = 200;
      ctx.body = profile;
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
});

module.exports = router;
