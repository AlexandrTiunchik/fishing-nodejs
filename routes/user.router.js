const Router = require('koa-router');
const passport = require('koa-passport');

const User = require('../schemas/user.schema');
const { ROUTERS } = require('../constants/routers.constant');

const router = new Router({
  prefix: ROUTERS.USER_ROUTE,
});

// @route GET api/profile/
// @desc get user data
// @access ADMIN, USER,
router.get('/',
  passport.authenticate('jwt', {session: false}),
  async (ctx) => {
    try {
      const { id } = ctx.state.user;

      const foundUser = await User.findById(id)
        .populate('profile');

      if (!foundUser) {
        const errorMessage = 'User not found';
        return ctx.app.emit('error', errorMessage, 400, ctx);
      }

      const user = {
        id: foundUser.id,
        profileId: foundUser.profile,
        loginName: foundUser.loginName,
        role: foundUser.role,
        avatar: foundUser.avatar,
        profile: foundUser.profile,
      };

      ctx.status = 200;
      ctx.body = user;
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
});

module.exports = router;
