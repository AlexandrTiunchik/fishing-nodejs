const Router = require('koa-router');
const passport = require('koa-passport');
const mongoose = require('mongoose');

const { ROUTERS } = require('../constants/routers.constant');
const Vote = require('../schemas/vote.schema');

const router = new Router({
  prefix: ROUTERS.VOTE_ROUTE,
});

// @route GET api/vote/
// @desc get news vote
// @access PUBLIC
router.get(
  '/:id',
  async (ctx) => {
    try {
      const { id } = ctx.params;

      const comments = await Vote
        .aggregate([
          {
            $match: {
              news: mongoose.Types.ObjectId(id),
            },
          },
          {
            $group: {
              _id: {
                answerKey: '$answerKey',
              },
              count: { $sum: 1 },
            },
          },
        ]);

      ctx.status = 200;
      ctx.body = comments;
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route POST api/vote/
// @desc create vote for news
// @access ADMIN, USER
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { answerKey } = ctx.request.body;
      const { user } = ctx.state;

      const newVote = new Vote({
        user: user.id,
        news: id,
        answerKey,
      });

      await newVote.save();

      ctx.status = 201;
      ctx.body = { message: 'Your vote counted' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route DELETE api/vote/
// @desc delete news vote
// @access ADMIN, USER
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { user } = ctx.state;

      await Vote.findOneAndRemove({
        _id: id,
        user: user.id,
      });

      ctx.status = 200;
      ctx.body = { message: 'Vote was delete successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

module.exports = router;
