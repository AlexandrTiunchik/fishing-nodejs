const Router = require('koa-router');
const passport = require('koa-passport');

const { ROUTERS } = require('../constants/routers.constant');
const NewsSchema = require('../schemas/news.schema');

const router = new Router({
  prefix: ROUTERS.NEWS_ROUTE,
});

// @route GET api/news/
// @desc Get news articles
// @access PUBLIC
router.get('/', async (ctx) => {
  try {
    const news = await NewsSchema.find(ctx.request.query);

    ctx.status = 200;
    ctx.body = news;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

// @route GET api/news/
// @desc Get news article
// @access PUBLIC
router.get('/:id', async (ctx) => {
  try {
    const { id } = ctx.params;

    const news = await NewsSchema.findById(id);

    ctx.status = 200;
    ctx.body = news;
  } catch (err) {
    ctx.app.emit('error', err, 400, ctx);
  }
});

// @route PUT api/news/
// @desc Redact news article
// @access ADMIN
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;
      const { title, text, titleImage, category } = ctx.request.body;

      const updateFish = {
        title,
        text,
        category,
        titleImage,
      };

      await NewsSchema.findByIdAndUpdate(
        id,
        { $set: updateFish },
      );

      ctx.status = 201;
      ctx.body = { message: 'News was update successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route POST api/news/
// @desc Create news article
// @access ADMIN
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { title, text, titleImage, category } = ctx.request.body;

      const newNews = new NewsSchema({
        title,
        text,
        category,
        titleImage,
      });

      await newNews.save();

      ctx.status = 201;
      ctx.body = { message: 'News was created successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

// @route DELETE api/news/
// @desc Delete news article
// @access ADMIN
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (ctx) => {
    try {
      const { id } = ctx.params;

      await NewsSchema.findByIdAndRemove(id);

      ctx.status = 200;
      ctx.body = { message: 'News was deleted successfully' };
    } catch (err) {
      ctx.app.emit('error', err, 400, ctx);
    }
  },
);

module.exports = router;
