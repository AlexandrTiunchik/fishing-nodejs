const DEV_KEY = require('./keys.dev');
const PROD_KEY = require('./keys.prod');

module.exports = process.env.NODE_ENV === 'production' ? PROD_KEY : DEV_KEY;
