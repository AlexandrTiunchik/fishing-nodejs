const ROOT_ROUTE = '/api';

exports.ROUTERS = {
  FISH_ROUTE: `${ROOT_ROUTE}/fish`,
  NEWS_ROUTE: `${ROOT_ROUTE}/news`,
  AUTHENTICATION_ROUTE: `${ROOT_ROUTE}/authentication`,
  COMMENT_ROUTE: `${ROOT_ROUTE}/comment`,
  VOTE_ROUTE: `${ROOT_ROUTE}/vote`,
  USER_ROUTE: `${ROOT_ROUTE}/user`,
  PROFILE_ROUTE: `${ROOT_ROUTE}/profile`,
};
