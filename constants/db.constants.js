const DB = {
  DEFAULT_IMAGE: 'https://dpmptsp.sulselprov.go.id/assets/file/blank.png',
  DEFAULT_AVATAR: 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1',
};

module.exports = DB;
