const registrationValidation = (ctx) => {
  ctx.checkBody('loginName', 'Login name must be longer than 5 and shorter than 12 characters').len(5, 12);
  ctx.checkBody('password', 'Password must be longer than 5 and shorter than 12 characters').len(5, 12);
  ctx.checkBody('email', 'Invalid Email').isEmail();
};

module.exports = registrationValidation;
