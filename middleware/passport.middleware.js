const passportJWT = require('passport-jwt');
const passport = require('koa-passport');

const User = require('../schemas/user.schema');
const KEYS = require('../configs/keys/keys');

const { Strategy, ExtractJwt } = passportJWT;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: KEYS.SECRET_OR_KEY,
};

passport.use(new Strategy(jwtOptions, (async (payload, done) => {
  try {
    const user = await User.findById(payload.id);
    done(null, user);
  } catch (err) {
    done(err);
  }
})));
