const error = (err, status, ctx) => {
  ctx.status = status;
  ctx.body = err.message || err;
};

module.exports = error;
