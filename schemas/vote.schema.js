const mongoose = require('mongoose');

const { Schema } = mongoose;

const voteSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  news: {
    type: Schema.Types.ObjectId,
    ref: 'news',
  },
  date: {
    type: Date,
    default: Date.now,
  },
  answerKey: {
    type: String,
    default: '',
  },
});

module.exports = Vote = mongoose.model('vote', voteSchema);
