const mongoose = require('mongoose');

const DB = require('../constants/db.constants');

const { Schema } = mongoose;

const profileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  name: {
    type: String,
    default: '',
  },
  surname: {
    type: String,
    default: '',
  },
  avatar: {
    type: String,
    default: DB.DEFAULT_AVATAR,
  },
  about: {
    type: String,
    default: '',
  },
  images: [String],
  socialLink: [
    {
      name: String,
      link: String,
    },
  ],
});

module.exports = User = mongoose.model('profile', profileSchema);
