const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  profile: {
    type: Schema.Types.ObjectId,
    ref: 'profile',
  },
  loginName: {
    type: String,
    default: '',
  },
  password: {
    type: String,
    default: '',
  },
  email: {
    type: String,
    default: '',
  },
  date: {
    type: Date,
    default: Date.now,
  },
  role: {
    type: String,
    default: 'user',
  },
});

module.exports = User = mongoose.model('user', userSchema);
