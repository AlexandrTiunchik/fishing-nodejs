const mongoose = require('mongoose');

const DB = require('../constants/db.constants');

const { Schema } = mongoose;

const newsSchema = new Schema({
  title: {
    type: String,
    default: '',
  },
  text: {
    type: String,
    default: '',
  },
  category: {
    type: String,
    default: '',
  },
  titleImage: {
    type: String,
    default: DB.DEFAULT_IMAGE,
  },
});

module.exports = News = mongoose.model('news', newsSchema);
