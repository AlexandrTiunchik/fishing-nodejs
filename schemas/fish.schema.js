const mongoose = require('mongoose');

const DB = require('../constants/db.constants');

const { Schema } = mongoose;

const fishSchema = new Schema({
  title: {
    type: String,
    default: '',
  },
  text: {
    type: String,
    default: '',
  },
  titleImage: {
    type: String,
    default: DB.DEFAULT_IMAGE,
  },
});

module.exports = Fish = mongoose.model('fish', fishSchema);
